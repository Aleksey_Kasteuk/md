const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        home: path.resolve('./public/index.js')
    },
    output: {
        path: __dirname + '/dist',
        filename: '[name].min.js',
        publicPath: '/dist/'
    },
    module: {
        loaders: [
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: [
                    { 
                        loader: 'css-loader', 
                        options: { 
                            importLoaders: 1,
                            sourceMap: true 
                        } 
                    },
                    { 
                        loader: 'postcss-loader', 
                        options: { 
                            sourceMap: true,
                            parser: 'PostCSS',
                            plugins: [
                                require('autoprefixer')({
                                    'browsers': [
                                      'last 2 version'
                                    ],
                                    grid: true
                                  }),
                              ]
                        }
                    },
                    { loader: 'less-loader', options: { sourceMap: true } }
                  ]
                })
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: [
                    { 
                        loader: 'css-loader', 
                        options: { 
                            importLoaders: 1, 
                            sourceMap: true 
                        }
                    }
                  ]
                })
            },
            {
                test: /\.(mp4|ogg|svg|eot|ttf|woff|woff2|jpg|png|otf)$/,
                loader: 'file-loader',
                query: {
                    publicPath: '/dist/'
                }
            }
        ]
    },
    resolve: {
        extensions: ['.js']
    },
    plugins: [
        new ExtractTextPlugin({ filename: '[name].css', allChunks: true }),
    ]
};