<?php
    require 'vendor/autoload.php';

    $app = new \Slim\App();
    $container = $app->getContainer();

    $container['view'] = function ($c) {
        $paths = ['./server/templates'];
        $view = new \Slim\Views\Twig($paths);
        $view->addExtension(new \Slim\Views\TwigExtension(
            $c['router'],
            $c['request']->getUri()
        ));
        return $view;
    };

    $app->get('/', function($request, $response) {
        $this->view->render($response, 'home.html');
    });

    $container['notFoundHandler'] = function ($c) {
        return function ($request, $response) use ($c) {
            if (preg_match('/^\/rest\//', $request->getUri()->getPath())) {
                return $response->withStatus(404)->withJson(array('error' => array('Invalid path')));
            } else {
                // return $c['view']->render($response->withStatus(404), 'pages/404.html');
                $app->redirect('/');
            }
        };
    };